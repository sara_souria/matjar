<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;

class OrdersController extends Controller
{
    protected function store(Request $request)
    {
        $product=Product::Find($request->input('product_id'));

        $order=$product->order()->create([
            "name"=>$request->input('name'),
            "tel"=>$request->input('tel'),
            "amount"=>$request->input('amount'),
            "value"=>$this->calculate_value($product,$request->input('amount')),
            "branch_id"=>$request->input('branch_id')            
        ]);
        return response()->json($order);

    }

    protected function calculate_value(Product $product,$amount)
    {        
        $item_price=$this->product_price($product);
        return $item_price*$amount;
    }

    protected function product_price(Product $product)
    {
        if($product->offer)
        {
            $percent=$product->offer->percentage;
            return $product->price-$product->price*$percent/100;
        }
        else
            return $product->price;
    }
}
