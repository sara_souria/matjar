<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;

class OffersController extends Controller
{
    public function index()
    {
        $offers=Offer::all();
        foreach($offers as $offer)
        {
            $offer->product;
        }
        return response()->json($offers,200);
    }
}
