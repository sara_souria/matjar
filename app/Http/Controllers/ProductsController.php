<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    protected function index()
    {
        $products=Product::all();
        foreach($products as $product)
        {
            $product->sort;
            $product->pictures;
        }
        return response()->json($products,200);
    }

    protected function show($id)
    {
        $product=Product::Find($id);
        $product->sort;
        $product->pictures;
        return response()->json($product,200);

    }
}
