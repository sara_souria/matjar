<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Poster;

class PostersController extends Controller
{
    protected function index()
    {
        $posters=Poster::all();
        return response()->json($posters,200);
    }
}
