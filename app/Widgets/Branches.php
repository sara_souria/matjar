<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class Branches extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \App\Models\Branch::count();
        $string = __('Branches');

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-leaf',
            'title'  => "{$count} {$string}",
            'text'   => __('WidgetLabel', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('WidgetBrowseBtn',['string'=>Str::lower($string)]),
                'link' => route('voyager.branches.index'),
            ],
            'image' => widget_image('office.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}
