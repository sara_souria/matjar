<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function sort()
    {
        return $this->belongsTo(Sort::class);
    }

    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function offer()
    {
        return $this->hasOne(Offer::class);
    }
}
