<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', [App\Http\Controllers\ProductsController::class,'index']);

Route::get('/products/{id}', [App\Http\Controllers\ProductsController::class,'show']);

Route::get('/posters', [App\Http\Controllers\PostersController::class,'index']);

Route::get('/offers', [App\Http\Controllers\OffersController::class,'index']);

Route::get('/shippings', [App\Http\Controllers\ShippingController::class,'index']);

Route::post('/order', [App\Http\Controllers\OrdersController::class,'store']);
